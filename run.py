import os
from dotenv import load_dotenv
import paramiko
import pandas as pd

# Read from .env file.
load_dotenv()
env = os.environ.get

HOSTNAME = env("ENDPOINT")
USERNAME = env("LOGIN")
PASSWORD = env("PASSWORD")

# Create SSH client.
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Establish SSH connection to remote server.
print("\nEstablishing SSH connection...")
ssh.connect(hostname=HOSTNAME, username=USERNAME, password=PASSWORD, port=22)
print("SSH connection complete.")

# Establish SFTP connection remote server.
print("\nEstablishing SFTP connection...")
sftp_client = ssh.open_sftp()
print("SFTP connection complete.")

# Download csv file.
print("\nDownloading csv file...")
sftp_client.chdir(env("HOME_DIR"))
sftp_client.get(env("DOWNLOAD_FILE_PATH"), "downloaded_file.csv")
print("Successfully downloaded 'downloaded_file.csv'.")

# Create Pandas dataframe with "downloaded_file.csv".
print("\nCreating Pandas dataframe...")
df = pd.read_csv(r"downloaded_file.csv")
print("Dataframe 'df' created.")
# print(df)

# Add new rows to the dataframe.
print("\nAdding new rows to 'df'...")
new_patients = [(
    "John",
    "Doe",
    "01/01/2001",
    "Male",
    "1 Main Street",
    "2 Main Street",
    "New York City",
    "NY",
    "12345",
    "john.doe@gmail.com",
    "(123)456-7890",
    "unique1",
    "accession1",
    "testkit1"
    ),
    (
    "Jane",
    "Doe",
    "02/02/2002",
    "Female",
    "3 Main Street",
    "4 Main Street",
    "New York City",
    "NY",
    "67890",
    "jane.doe@gmail.com",
    "(098)765-4321",
    "unique2",
    "accession2",
    "testkit2"
    )
]
df2 = pd.DataFrame(new_patients, columns=list(df.columns))
# print(df2)
df = pd.concat([df, df2], ignore_index=True)
print("New rows added.")
# print(df)

# Write to "output.csv".
print("\nWriting dataframe to 'output.csv'...")
df.to_csv(r"output_file.csv")
print("Successfully written dataframe to 'output.csv'.")

# Upload "output.csv" to remote server as "test_output.csv".
print("\nUploading 'output_file.csv' to remote server as 'test_output.csv'...")
sftp_client.put("output_file.csv", "orders/test_output.csv")
print("Successfully uploaded  ")
# sftp_client.get("orders/test_output.csv", "test.csv")

# Close SFTP connection.
print("\nTerminating SFTP connection...")
sftp_client.close()
print("SFTP connection terminated.")

# Close SSH connection.
print("\nTerminating SSH connection...")
ssh.close()
print("SSH connection terminated.")

print('\nOperation complete.')