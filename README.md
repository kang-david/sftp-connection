A script that

    1. downloads a csv file from a remote SFTP endpoint using Paramiko,
    2. modifies the contents with Pandas, and
    3. uploads the output file to the remote server.

Once you have installed the dependencies and added the environment variables inside a new ".env" file, simply run "run.py" in the console.